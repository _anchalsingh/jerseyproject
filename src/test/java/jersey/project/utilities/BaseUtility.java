package jersey.project.utilities;

import javax.ws.rs.core.MediaType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeTest;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class BaseUtility {

	public static Properties properties;
	public static Client client;
	public static String userName = "";
	public static String password = "";
	public static WebResource request;
	public static ClientResponse response;
	public static String accesstoken;
	public static String loginBody = "{\"email\": \"admin@mailinator.com\",\"password\": \"admin123#\",\"remember-me\": true}";
	

	@BeforeTest
	public static void setupHost() throws IOException {
		properties = BaseUtility.readPropertiesFile();
		client = Client.create();
		request = client.resource(properties.getProperty("Baseurl"));
		userName = properties.getProperty("UserName");
		password = properties.getProperty("PassWord");
	}
	public static Properties readPropertiesFile() throws IOException {
		properties = new Properties();
		FileInputStream fileinput;
		
		try {
			fileinput = new FileInputStream(System.getProperty("user.dir")+ "\\src\\test\\resources\\config.properties");
			properties.load(fileinput);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Properties file is not found");
		}
		return properties;
	}
	
	public static Object readJsonFile(String fileName) throws IOException, ParseException {
		FileReader reader = new FileReader(System.getProperty("user.dir")+"\\src\\test\\resources\\TestData\\"+fileName);
		JSONParser jsonparser=new JSONParser();
		Object obj = jsonparser.parse(reader);
		return obj;
	}
	
	public static String randomString() {
		return "Anchal" + RandomStringUtils.randomAlphabetic(4);
	}

	public static String getAccessToken() {
		response = request.path("/v1/auth/login").type(MediaType.APPLICATION_JSON).post(ClientResponse.class, loginBody);
		String output = response.getEntity(String.class);
		String accesstoken = "$.access_token";
		DocumentContext namejsonContext = JsonPath.parse(output);
		return namejsonContext.read(accesstoken);
	}

}
