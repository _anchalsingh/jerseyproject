package jesry.project.test;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.ClientResponse;

import jersey.project.utilities.BaseUtility;

public class EventTypeTest extends BaseUtility {

	List<String> listEventTypeID;
	String eventTypeID;

	@Test(description = "To get the list of event type.", priority = 1)
	public void getEventTypeList() {
		response = request.path("/v1/event-types").queryParam("sort", "name").accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		String reposeoutput = response.getEntity(String.class);
		System.out.println("The Repose body : \n" + reposeoutput);
		String listeventtype = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(reposeoutput);
		listEventTypeID = namejsonContext.read(listeventtype);
		System.out.println("List of event ID  : " + listEventTypeID);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}
     @Test(description = "To create event type.", priority = 2)
	public void createEventType() {
		JsonObject requestJson = new JsonObject();
		JsonObject data = new JsonObject();
		JsonObject attribute = new JsonObject();
		attribute.addProperty("name", properties.getProperty("eventtypename"));
		data.add("attributes", attribute);
		data.addProperty("type", properties.getProperty("eventtype"));
		requestJson.add("data", data);
		response=request.path("/v1/event-types").header("Content-Type", "application/vnd.api+json")
		.header("Authorization", "JWT " + getAccessToken()).post(ClientResponse.class,requestJson.toString());
		String resposeoutput = response.getEntity(String.class);
		System.out.println("The Request Body : " + requestJson.toString());
		System.out.println("The Respose body : " + resposeoutput);
		String evnttypeid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposeoutput);
		eventTypeID = namejsonContext.read(evnttypeid);
		System.out.println("Event type : " + eventTypeID);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 201);
	}
     @Test(description = "To get the Event Type details.", priority = 3)
 	public void getEventTypeDetail() {
 		response=request.path("/v1/event-types/"+eventTypeID).accept("application/vnd.api+json")
 		.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
 		String output = response.getEntity(String.class);
 		System.out.println("The Respose body : \n" + output);
 		int statusCode = response.getStatus();
 		Assert.assertEquals(statusCode, 200);
 	}
 	@Test(description ="To delete the Event Type details.", priority = 4)
 	public void deleteEventTypeDetail() {
 		response=request.path("/v1/event-types/"+eventTypeID).accept("application/vnd.api+json")
 		.header("Authorization", "JWT " + getAccessToken()).delete(ClientResponse.class);
 		String output = response.getEntity(String.class);
 		System.out.println("The Respose body : \n" + output);
 		int statusCode = response.getStatus();
 		Assert.assertEquals(statusCode, 200);
 		Assert.assertEquals(output.contains("Object successfully deleted"), true);
 	}
}
