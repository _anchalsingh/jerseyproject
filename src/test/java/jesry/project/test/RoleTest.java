package jesry.project.test;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.ClientResponse;

import jersey.project.utilities.BaseUtility;

public class RoleTest extends BaseUtility {

	String roleID;
	List<String> listRoleID;
	String roleName = BaseUtility.randomString();
	String rtitle = "Co-organizer";

	@Test(description = "To get the List of Role.", priority = 1)
	public void getListRoles() {
		response = request.path("/v1/roles").queryParam("sort", "name").accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		//System.out.println("Request Body: " + response.toString());
		String receivedResponse = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + receivedResponse);
		String ListID = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(receivedResponse);
		listRoleID = namejsonContext.read(ListID);
		System.out.println("Role ID List  : " + listRoleID);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}

	@Test(description = "To create the role.", priority = 2)
	public void createRole() {
		JsonObject requestJson = new JsonObject();
		JsonObject data = new JsonObject();
		JsonObject attribute = new JsonObject();
		attribute.addProperty("name", roleName);
		attribute.addProperty("title-name", rtitle);
		data.add("attributes", attribute);
		data.addProperty("type", "role");
		requestJson.add("data", data);
		response=request.path("/v1/roles").header("Content-Type", "application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).post(ClientResponse.class , requestJson.toString());
		System.out.println("The Request Body : "+requestJson.toString());
		String resposeoutput = response.getEntity(String.class);
		System.out.println("The Respose body : "+resposeoutput);
		String roleid="$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposeoutput);
		roleID = namejsonContext.read(roleid);
		System.out.println("The ROLE ID : "+roleID);
		int statusCode =response.getStatus();
		Assert.assertEquals(statusCode, 201);
	}
	@Test(description = "To get the role details.", priority = 3)
	public void getRoleDetail() {
		response=request.path("/v1/roles/"+roleID).accept("application/vnd.api+json")
		.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		String receivedResponse = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + receivedResponse);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}
	@Test(description = "To delete the role detail.", priority = 4)
	public void deleteRoleDetail() {
		response=request.path("/v1/roles/"+roleID).accept("application/vnd.api+json")
		.header("Authorization", "JWT " + getAccessToken()).delete(ClientResponse.class);
		String receivedResponse = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + receivedResponse);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(receivedResponse.contains("Object successfully deleted"), true);
	}
}
