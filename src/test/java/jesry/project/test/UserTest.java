package jesry.project.test;

import java.io.IOException;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.ClientResponse;

import jersey.project.utilities.BaseUtility;

public class UserTest extends BaseUtility {
	public static String ListuserId;
	public static String userid;

	@Test(description = "To get the List of users.", priority = 1)
	public void getUsersList() {
		response = request.path("/v1/users").accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		System.out.println("Request Body: " + response.toString());
		String responseBody = response.getEntity(String.class);
		ListuserId = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(responseBody);
		System.out.println("User ID List : " + namejsonContext.read(ListuserId));
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}

	@Test(description = "To create users", priority = 2)
	public void createUser() throws IOException, ParseException {
		Object reader = BaseUtility.readJsonFile("CreateUser.Json");
		System.out.println("The Request Body : \n" + reader.toString());
		response = request.path("/v1/users").header("Content-Type", "application/vnd.api+json")
				.post(ClientResponse.class, reader.toString());
		String output = response.getEntity(String.class);
		System.out.println("The Respose Body : \n" + output);
		int statusCode = response.getStatus();
		String userId = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(output);
		System.out.println("User ID : " + namejsonContext.read(userId));
		userid = namejsonContext.read(userId);
		Assert.assertEquals(statusCode, 201);
	}
		
	@Test(description = "To get the user details.", priority = 3)
	public void getUserDetail() {
		response = request.path("/v1/users/" + userid).accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		System.out.println("Request Body: " + response.toString());
		String responseBody = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + responseBody);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}

	@Test(description = "To delete the users.", priority = 4)
	public void deleteUser() {
		response = request.path("/v1/users/"+userid).accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).delete(ClientResponse.class);
		String responseBody = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + responseBody);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(responseBody.contains("Object successfully deleted"), true);
	}
}
