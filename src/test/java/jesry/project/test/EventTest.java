package jesry.project.test;

import java.io.IOException;

import javax.ws.rs.core.MediaType;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.ClientResponse;

import jersey.project.utilities.BaseUtility;

public class EventTest extends BaseUtility {
	public static String eventID;
	public static String event_Identifier;
	String eventName = BaseUtility.randomString();

	@Test(description = "To get the List of Events",priority = 1)
	public void getListEvents() {
		response = request.path("/v1/events").queryParam("sort", "identifier").accept("application/vnd.api+json")
				.header("Authorization", "JWT " + getAccessToken()).get(ClientResponse.class);
		System.out.println("Request Body: " + response.toString());
		String output = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + output);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}

	@Test(description = "Verify to create new event", priority = 2)
	public void createEvent() throws IOException, ParseException {
		Object reader = BaseUtility.readJsonFile("CreateEvent.Json");
	//	System.out.println("The Request Body : \n" + reader.toString());
		response = request.path("/v1/events").queryParam("sort", "identifier")
				.header("Content-Type", "application/vnd.api+json").header("Authorization", "JWT " + getAccessToken())
				.post(ClientResponse.class, reader.toString());
		String receivedresponse = response.getEntity(String.class);
		System.out.println("The Response Body : \n" + receivedresponse);
		String eventid = "$.data.id";
		String eventIdentifier = "$.data.attributes.identifier";
		DocumentContext namejsonContext = JsonPath.parse(receivedresponse);
		eventID = namejsonContext.read(eventid);
		event_Identifier =namejsonContext.read(eventIdentifier);
		System.out.println("The event id : " + eventID);
		System.out.println("The event identifier : " + event_Identifier);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 201);
	}

	@Test(description = "To get the event details." ,priority = 3)
	public void getEventDetail() throws IOException, ParseException {
		response=request.path("/v1/events/"+eventID).accept("application/vnd.api+json").header("Authorization", "JWT " + getAccessToken())
				.get(ClientResponse.class);
		System.out.println("Request Body: " + response.toString());
		String output = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + output);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}
	
	@Test(description = "To delete the event.", priority = 4)
	public void deleteEvent() throws IOException, ParseException {
		response=request.path("/v1/events/"+eventID).accept("application/vnd.api+json").header("Authorization", "JWT " + getAccessToken())
				.delete(ClientResponse.class);
		System.out.println("Request Body: " + response.toString());
		String receivedResponse = response.getEntity(String.class);
		System.out.println("The Respose body : \n" + receivedResponse);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(receivedResponse.contains("Object successfully deleted"), true);
	}
}
