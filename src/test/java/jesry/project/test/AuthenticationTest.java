package jesry.project.test;

import javax.ws.rs.core.MediaType;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;

import jersey.project.utilities.BaseUtility;

public class AuthenticationTest extends BaseUtility {

	@Test(description = "To Authenticate and generate token", priority = 1)
	public void verifyAuthenticateLogin() {
		response = request.path("/v1/auth/login").type(MediaType.APPLICATION_JSON)
				.header("Content-Type", "application/json").post(ClientResponse.class, loginBody);
		String output = response.getEntity(String.class);
		System.out.println("The Respose Body : \n" + output);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}
	
	@Test(description= "To Authenticate with remember me", priority = 2)
	public void verifyAuthenticateLoginRemember() {
		response = request.path("/v1/auth/login").type(MediaType.APPLICATION_JSON)
				.header("Content-Type", "application/json").post(ClientResponse.class, loginBody);
		String output = response.getEntity(String.class);
		System.out.println("The Respose Body : \n" + output);
		int statusCode = response.getStatus();
		Assert.assertEquals(statusCode, 200);
	}

}
